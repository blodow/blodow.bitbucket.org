$(document).ready( function() {

  var consoleTimeout;

  $('.minicolors').each( function() {
    //
    // Dear reader, it's actually much easier than this to initialize 
    // miniColors. For example:
    //
    //  $(selector).minicolors();
    //
    // The way I've done it below is just to make it easier for me 
    // when developing the plugin. It keeps me sane, but it may not 
    // have the same effect on you!
    //
    $(this).minicolors({
      control: $(this).attr('data-control') || 'hue',
      defaultValue: $(this).attr('data-default-value') || '',
      inline: $(this).hasClass('inline'),
      letterCase: $(this).hasClass('uppercase') ? 'uppercase' : 'lowercase',
      opacity: $(this).hasClass('opacity'),
      position: $(this).attr('data-position') || 'default',
      styles: $(this).attr('data-style') || '',
      swatchPosition: $(this).attr('data-swatch-position') || 'left',
      textfield: !$(this).hasClass('no-textfield'),
      theme: $(this).attr('data-theme') || 'default',
      change: function(hex, opacity) {

        // Generate text to show in console
        text = hex ? hex : 'transparent';
        if( opacity ) text += ', ' + opacity;
        text += ' / ' + $(this).minicolors('rgbaString');

        var red = $(this).minicolors('rgbObject').r;
        var green= $(this).minicolors('rgbObject').g;
        var blue = $(this).minicolors('rgbObject').b;

        if ($(this).attr('varname') == "min_color")
          min_color = new Float32Array ([red/255.0,green/255.0,blue/255.0]);
        if ($(this).attr('varname') == "max_color")
          max_color = new Float32Array ([red/255.0,green/255.0,blue/255.0]);
      }
    });

  });
});

