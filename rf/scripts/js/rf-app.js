if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

var gl;

var ui_visible = true;

var container, stats;
var renderer, camera, scene, material, plane;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var limit_to_10Hz = false;
var animate_handle;

// timing
var lastTime = 0, time = 0;

// function editing
var originalFunction = "";
var func = originalFunction;
var func_edit;

// subimage position
var delta_x = 0, delta_y = 0;
var zoom = 10.0;

// shader stuff
var vertShader;
var uniforms;

// colors
var min_color = new Float32Array([0.0,0.0,0.0]);
var max_color = new Float32Array([1.0,1.0,1.0]);

function init() {
  container = document.createElement( 'div' );
  document.body.appendChild( container );

  camera = new THREE.OrthographicCamera(-1, 1, 1, -1, -10, 10);
  camera.position.x = 0.0;
  camera.position.y = 0.0;
  camera.position.z = -5.0;
 
  scene = new THREE.Scene();
  plane = new THREE.Mesh( new THREE.PlaneGeometry (2, 2, 1, 1), material );
  plane.position.x = 0;
  plane.position.y = 0;
  plane.position.z = 0;
  scene.add( plane );

  // Setup renderer and effects
  renderer = new THREE.WebGLRenderer( { clearColor: 0x0f0f0f, clearAlpha: 1, antialias: false } );
  renderer.setSize( window.innerWidth, window.innerHeight );
  gl = renderer.getContext();

  renderer.domElement.setAttribute( 'tabindex', 0 );
  renderer.domElement.focus();
  container.appendChild( renderer.domElement );

//  stats = new Stats();
//  stats.domElement.style.position = 'absolute';
//  stats.domElement.style.bottom = '5px';
//  stats.domElement.style.right = '5px';
//  settingscontainer = document.getElementById('settings');
//  settingscontainer.appendChild( stats.domElement );

  // Setup listeners
  window.addEventListener( 'resize', onWindowResize, false );
  container.addEventListener( 'mousemove', onDocumentMouseMove, false );
  container.addEventListener( 'mouseup', onDocumentMouseUp, false );
  container.addEventListener( 'mouseout', onDocumentMouseOut, false );
  container.addEventListener( 'mousedown', onDocumentMouseDown, false );
  container.addEventListener( 'touchstart', onDocumentTouchStart, false );
  container.addEventListener( 'touchmove', onDocumentTouchMove, false );
  container.addEventListener( 'touchend', onDocumentTouchMove, false );
  container.addEventListener( 'keydown', onKeyDown, false );
  container.addEventListener('DOMMouseScroll', onDocumentMouseWheel, false);
  container.addEventListener('mousewheel', onDocumentMouseWheel, false);
  // load shaders
  vertShader = document.getElementById('simplevertexshader').innerHTML;

  uniforms = {
    zoom: {type: 'f', value: zoom},
    delta_x: {type: 'f', value: delta_x},
    delta_y: {type: 'f', value: delta_y},
    time: {type: 'f', value: 0.0},
    aspect: {type: 'f', value: 1.0},
    min_color: {type: 'fv1', value: min_color},
    max_color: {type: 'fv1', value: max_color},
  };
  observeFuncEdit ();

  if(limit_to_10Hz)
    animate_handle = setInterval (animate, 100);
  setInterval (observeFuncEdit, 200);
  animate ();
}

function loadFragShader (fragShader) {
  material = new THREE.ShaderMaterial ({
    uniforms: uniforms,
    attributes: {},
    vertexShader: vertShader,
    fragmentShader: fragShader,
    transparent: true
  });
}

function animate() {
  if (!limit_to_10Hz)
    requestAnimationFrame( animate );
  var timeNow = new Date().getTime();
  if (lastTime != 0) {
    var elapsed = timeNow - lastTime;
    time += elapsed / 1000.0;
  }
  lastTime = timeNow;

  uniforms.zoom.value = zoom;
  uniforms.delta_x.value = delta_x;
  uniforms.delta_y.value = delta_y;
  uniforms.aspect.value = windowHalfX / windowHalfY;
  uniforms.time.value = time;
  uniforms.min_color.value = min_color;
  uniforms.max_color.value = max_color;

  render();
  //stats.update();
}

function render() {
  renderer.render( scene, camera );
}

///////////////////////////////////////////////
// Event listeners
///////////////////////////////////////////////
var mouseDown = false;
var lastMouseX = null;
var lastMouseY = null;
function onDocumentMouseMove( event ) {
  if (!mouseDown) {
    return;
  }
  var newX = event.clientX;
  var newY = event.clientY;

  delta_x += ((newX - lastMouseX)/(2*windowHalfX)) * zoom;
  delta_y -= ((newY - lastMouseY)/(2*windowHalfY)) * zoom;
  lastMouseX = newX;
  lastMouseY = newY;
}
function onDocumentMouseUp( event ) {
  mouseDown = false;
}
function onDocumentMouseOut( event ) {
  mouseDown = false;
}
function onDocumentMouseDown( event ) {
  mouseDown = true;
  lastMouseX = event.clientX;
  lastMouseY = event.clientY;
}

// touch events
function onDocumentTouchStart( event ) {
  if ( event.touches.length == 1 ) {
    event.preventDefault();
    mouseDown = true;
    lastMouseX = event.clientX;
    lastMouseY = event.clientY;
  }
}
function onDocumentTouchMove( event ) {
  if ( event.touches.length == 1 ) {
    event.preventDefault();
    mouseDown = false;
  }
}

// wheel event
function onDocumentMouseWheel( event ) {    
  var delta = ((typeof event.wheelDelta != "undefined")?(-event.wheelDelta):event.detail);
  delta = 100 * ((delta>0)?1:-1);

  if (delta < 0)
    zoom *= 0.9;
  else if (delta > 0)
    zoom *= 1.1;
}

function onWindowResize( event ) {
  windowHalfX = window.innerWidth / 2;
  windowHalfY = window.innerHeight / 2;
  uniforms.aspect.value = windowHalfX / windowHalfY;
  //camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight );
}				

function recompileShader( shaderSource ) {
  loadFragShader (shaderSource);
  renderer.initMaterial( material, scene.__lights, scene.fog, plane );
  var status = gl.getProgramParameter( material.program, gl.LINK_STATUS );
  if ( status ) {
    document.getElementById('func').style.background = '#121212';
    plane.material = material;
    plane.material.needsUpdate = true;
  } else {
    document.getElementById('func').style.background = '#f00';
  }
}

function onKeyDown(event){
  if(event.keyCode == 72 || event.keyCode == 104) 
    showHideUI();
}

function observeFuncEdit ()
{
  if (!func_edit)
    func_edit = document.getElementById('func_edit');
  val = func_edit.value;
  if (val == func)
    return;
  else
  {
    func = val;
    cleanfunc = func.replace (/([^.0-9])(\d+)([^.0-9])/,"$1$2.0$3");
    recompileShader(document.getElementById('rfshader').innerHTML.replace('FUNCSTRING', cleanfunc));
  }
}

function showHideAbout() {
  if (document.getElementById('about').style.display == "block") {
    document.getElementById('about').style.display = "none";
  } else {
    document.getElementById('about').style.display = "block";
  }
}
function showHideSettings() {
  if (document.getElementById('settings').style.display == "block") {
    document.getElementById('settings').style.display = "none";
  } else {
    document.getElementById('settings').style.display = "block";
  }
}
function showHideFunc() {
  if (document.getElementById('func').style.display == "block") {
    document.getElementById('func').style.display = "none";
  } else {
    document.getElementById('func').style.display = "block";
  }
}
function showHideUI(){
  if(ui_visible){
    document.getElementById('settingslink').style.display = 'none';
    document.getElementById('settings').style.display = 'none';
    document.getElementById('funclink').style.display = 'none';
    document.getElementById('func').style.display = 'none';
    document.getElementById('aboutlink').style.display = 'none';
    document.getElementById('about').style.display = 'none';
    document.getElementById('info').style.display = 'none';
    document.getElementById('myname').style.display = 'none';
    //stats.domElement.style.display = 'none';
    renderer.domElement.style.cursor = "none";
    ui_visible = false;
  }
  else {
    document.getElementById('settingslink').style.display = 'block';
    document.getElementById('funclink').style.display = 'block';
    document.getElementById('aboutlink').style.display = 'block';
    document.getElementById('info').style.display = 'block';
    document.getElementById('myname').style.display = 'block';
    //stats.domElement.style.display = 'block';
    renderer.domElement.style.cursor = "";
    ui_visible = true;
  }
}
function handleLimitClick(cb) {
  setTimeout(function() {
    limit_to_10Hz = cb.checked;
    if (limit_to_10Hz)
      animate_handle = setInterval (animate, 100);
    else
    {
      window.clearInterval (animate_handle);
      requestAnimationFrame( animate );
    }
  }, 0);
}
